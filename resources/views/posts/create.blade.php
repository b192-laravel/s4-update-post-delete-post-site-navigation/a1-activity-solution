{{-- this means that codes inside this will be used to the main component which is app.blade in view/layouts/ --}}
@extends('layouts.app')

{{-- 'content' matched the @yield in main tag inside app.blade --}}
@section('content')
    <form method="POST" action="/posts">
      @csrf
      <div class="form-group">
        <label for="title">Title:</label>
        <input type="text" class="form-control" id="title" name="title">
      </div>
      <div class="form-group">
        <label for="content">Content:</label>
        <textarea class="form-control" id="content" name="content" rows="3"></textarea>
      </div>
      <div class="mt-2">
      	<button type="submit" class="btn btn-primary">Create Post</button>
  	  </div>
    </form>
@endsection